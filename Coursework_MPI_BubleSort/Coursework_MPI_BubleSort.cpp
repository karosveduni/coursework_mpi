#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

constexpr auto SIZE_ARRAY = 10000;

#pragma region EN
/* swap entries in array v at positions i and j; 
    used by bubblesort this improves performance;*/
#pragma endregion
#pragma region RU
/*�������� ������� ������ � ������� v � �������� i � j;
    ������������ ����������� ����������� ��� ��������� ������������������;*/
#pragma endregion
#pragma region UA
/*������� ������ ������ � ����� v � �������� i �� j;
    ��������������� ������������ �����������, �� ������� ��������������;*/
#pragma endregion
static inline void swap(int* v, int i, int j)
{
    int t = v[i];
    v[i] = v[j];
    v[j] = t;
}

#pragma region EN
/* (bubble) sort array v; array is of length n */
#pragma endregion
#pragma region RU
/*(�����������) ������ ���������� v; ������ ����� ����� n*/
#pragma endregion
#pragma region UA
/*(���������) ���������� ������ v; ����� �� ������� n*/
#pragma endregion
void bubblesort(int* v, int n)
{
    int i, j;
    for (i = n - 2; i >= 0; i--)
        for (j = 0; j <= i; j++)
            if (v[j] > v[j + 1])
                swap(v, j, j + 1);
}

#pragma region EN
/* merge two sorted arrays v1, v2 of lengths n1, n2, respectively */
#pragma endregion
#pragma region RU
/*���������� ��� ��������������� ������� v1, v2 ����� n1, n2 ��������������*/
#pragma endregion
#pragma region UA
/*��'������ ��� ������������ ������ v1, v2 ������� n1, n2 ��������*/
#pragma endregion
int* merge(int* v1, int n1, int* v2, int n2)
{
    int* result = (int*)malloc((n1 + n2) * sizeof(int));
    int i = 0;
    int j = 0;
    int k;
    for (k = 0; k < n1 + n2; k++) {
        if (i >= n1) {
            result[k] = v2[j];
            j++;
        }
        else if (j >= n2) {
            result[k] = v1[i];
            i++;
        }
        else if (v1[i] < v2[j]) {
            result[k] = v1[i];
            i++;
        }
        else {
            result[k] = v2[j];
            j++;
        }
    }
    return result;
}


int main(int argc, char** argv)
{
    int n = 10000;
    int count, size;
    int* chunk;
    int o;
    int* other;
    int step;
    int p, id;
    MPI_Status status;
    double elapsed_time;
    int* array = new int[SIZE_ARRAY];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    if (id == 0) {
        srand(time(NULL));
        for (int i = 0; i < SIZE_ARRAY; i++)
        {
            array[i] = rand() % SIZE_ARRAY + 1;
        }
    }

#pragma region EN
    // start the timer
#pragma endregion
#pragma region RU
    /*��������� ������*/
#pragma endregion
#pragma region UA
    /*��������� ������*/
#pragma endregion
    MPI_Barrier(MPI_COMM_WORLD);
    elapsed_time = -MPI_Wtime();

#pragma region EN
    // broadcast size
#pragma endregion
#pragma region RU
    /*������ ����������*/
#pragma endregion
#pragma region UA
    /*����� ����������*/
#pragma endregion
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

#pragma region EN
    // compute chunk size
#pragma endregion
#pragma region RU
    /*��������� ������ �����*/
#pragma endregion
#pragma region UA
    /*��������� ����� ���������*/
#pragma endregion
    count = n / p; 
    if (n % p) count++;

#pragma region EN
    // scatter data
#pragma endregion
#pragma region RU
    /*������� ������*/
#pragma endregion
#pragma region UA
    /*��������� ����*/
#pragma endregion
    chunk = (int*)malloc(count * sizeof(int));
    MPI_Scatter(&array, count, MPI_INT, chunk, count, MPI_INT, 0, MPI_COMM_WORLD);
    
#pragma region EN
    // compute size of own chunk and sort it
#pragma endregion
#pragma region RU
    /*��������� ������ ������������ ����� � ������������� ���*/
#pragma endregion
#pragma region UA
    /*��������� ����� �������� ��������� �� ����������� ����*/
#pragma endregion
    size = (n >= count * (id + 1)) ? count : n - count * id;
    bubblesort(chunk, size);

#pragma region EN
    // up to log_2 p merge steps
#pragma endregion
#pragma region RU
    /*�� log2 p ����� �������*/
#pragma endregion
#pragma region UA
    /*�� log2 p ����� ������*/
#pragma endregion
    for (step = 1; step < p; step = 2 * step) {
        if (id % (2 * step) != 0) {
#pragma region EN
    // id is no multiple of 2*step: send chunk to id-step and exit loop
#pragma endregion
#pragma region RU
    /*id �� ������ 2 * step: ��������� �������� �� id-��� � ����� �� �����*/
#pragma endregion
#pragma region UA
    /*id �� ������� 2*step: ��������� �������� �� id-step � ����� � �����*/
#pragma endregion
            MPI_Send(chunk, size, MPI_INT, id - step, 0, MPI_COMM_WORLD);
            break;
        }
#pragma region EN
    //id is multiple of 2*step: merge in chunk from id+step (if it exists)
#pragma endregion
#pragma region RU
    /*id ������ 2 * step: ���������� � ���� �� id + step (���� �� ����������)*/
#pragma endregion
#pragma region UA
    /*id ������� 2*step: �ᒺ����� � ������ �� id+step (���� �� ����)*/
#pragma endregion
        if (id + step < p) {
#pragma region EN
    // compute size of chunk to be received
#pragma endregion
#pragma region RU
    /*��������� ������ ����������� �����*/
#pragma endregion
#pragma region UA
    /*��������� ����� ���������, ���� ���� ��������*/
#pragma endregion
            o = (n >= count * (id + 2 * step)) ? count * step : n - count * (id + step);
#pragma region EN
    // receive other chunk
#pragma endregion
#pragma region RU
    /*�������� ������ �����*/
#pragma endregion
#pragma region UA
    /*�������� ����� ������*/
#pragma endregion
            other = (int*)malloc(o * sizeof(int));
            MPI_Recv(other, o, MPI_INT, id + step, 0, MPI_COMM_WORLD, &status);
#pragma region EN
    // merge and free memory
#pragma endregion
#pragma region RU
    /*���������� � ���������� ������*/
#pragma endregion
#pragma region UA
    /*��'������ � �������� ���'���*/
#pragma endregion
            array = merge(chunk, size, other, o);
            free(chunk);
            free(other);
            chunk = array;
            size = size + o;
        }
    }

#pragma region EN
    // stop the timer
#pragma endregion
#pragma region RU
    /*���������� ������*/
#pragma endregion
#pragma region UA
    /*�������� ������*/
#pragma endregion
    elapsed_time += MPI_Wtime();

    if (id == 0) {
        
        int index_1 = -1;

        printf("Array after sorting:\n");

        while (++index_1 < SIZE_ARRAY)
        {
            printf("%i\t", &array[index_1]);
        }

        printf("\n%i procs: %f secs\n", p, elapsed_time);
    }

    MPI_Finalize();
    return 0;
}

