#include <iostream>
#include <chrono>

constexpr auto SIZE_ARRAY = 10000;

void InitializeArray(int* array, int size);
void BulbashkovaSortingMatrix(int* array, int size);
void PrintArray(int* array, int size, char* message);
void PrintTime(std::chrono::steady_clock::time_point start, std::chrono::steady_clock::time_point finish);

int main()
{
    char message[] = "\nArray after sorting:\n";
    char message_2[] = "Array before sorting:\n";

    int* array = new int[SIZE_ARRAY];

    InitializeArray(array, SIZE_ARRAY);

    PrintArray(array, SIZE_ARRAY, message_2);

    std::chrono::steady_clock::time_point start = std::chrono::high_resolution_clock::now();

    BulbashkovaSortingMatrix(array, SIZE_ARRAY);

    std::chrono::steady_clock::time_point finish = std::chrono::high_resolution_clock::now();

    PrintArray(array, SIZE_ARRAY, message);

    PrintTime(start, finish);
}

void InitializeArray(int* array, int size)
{
    srand(time(NULL));
    for (int i = 0; i < size; i++)
    {
        array[i] = rand() % SIZE_ARRAY;
    }
}

void BulbashkovaSortingMatrix(int* array, int size)
{
    int temp = 0;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (array[i] < array[j])
            {
                temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
}

void PrintArray(int* array, int size, char* message)
{
    int index_1 = -1;

    printf(message);

    while (++index_1 < size)
    {
        printf("%i\t", array[index_1]);
    }
}

void PrintTime(std::chrono::steady_clock::time_point start, std::chrono::steady_clock::time_point finish)
{
    std::chrono::duration<double> elapsed = finish - start;
    printf("\nElapse time: %f;", elapsed.count());
}
