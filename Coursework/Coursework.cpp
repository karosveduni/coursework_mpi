#include <stdio.h>
#include <math.h>
#include <chrono>

int factorial(int f);
void PrintTime(std::chrono::steady_clock::time_point start, std::chrono::steady_clock::time_point finish);

int main()
{
    const float x = 0.7f;
    double S = 0.00;

    std::chrono::steady_clock::time_point start = std::chrono::high_resolution_clock::now();

    for (int i = 2; i < 21; i++)
    {
        float timevalue = i * x;
        S += factorial(i - 1) / cos(timevalue) + log(x);
    }

    std::chrono::steady_clock::time_point finish = std::chrono::high_resolution_clock::now();

    printf("sum = %f", S);
    
    PrintTime(start, finish);

    return 0;
}

int factorial(int f)
{
    if (f == 0 || f == 1)
        return 1;
    return f * factorial(f - 1);
}

void PrintTime(std::chrono::steady_clock::time_point start, std::chrono::steady_clock::time_point finish)
{
    std::chrono::duration<double> elapsed = finish - start;
    printf("\nElapse time: %f;", elapsed.count());
}
