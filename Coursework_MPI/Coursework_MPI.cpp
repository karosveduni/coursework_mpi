#include <mpi.h>
#include <stdio.h>
#include <math.h>

constexpr auto SIZE_ARRAY = 19;

double array[] = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,18, 19, 20 };

#pragma region EN
    // Temporary array for slave process
#pragma endregion
#pragma region RU
    // ��������� ������ ��� ������������ ��������
#pragma endregion
#pragma region UA
    // ���������� ����� ��� ��������������� �������
#pragma endregion
double array_2[1000];

const float x = 0.7f;

int factorial(int f)
{
    if (f == 0 || f == 1)
        return 1;
    return f * factorial(f - 1);
}

int main(int argc, char* argv[])
{

    int pid, size_p,
        elements_per_process,
        n_elements_recieved;

    double elapsed_time = 0.00;

    MPI_Status status;

#pragma region EN
    // Creation of parallel processes
#pragma endregion
#pragma region RU
    // �������� ������������ ���������
#pragma endregion
#pragma region UA
    // ��������� ����������� �������
#pragma endregion
    MPI_Init(&argc, &argv);

#pragma region EN
    // find out process ID, and how many processes were started
#pragma endregion
#pragma region RU
    // ������ ������������� �������� � ������� ��������� ���� ��������
#pragma endregion
#pragma region UA
    // �������� ������������� ������� �� ������ ������� ���� ��������
#pragma endregion
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);
    MPI_Comm_size(MPI_COMM_WORLD, &size_p);

#pragma region EN
    // start the timer
#pragma endregion
#pragma region RU
    // ��������� ������
#pragma endregion
#pragma region UA
    // ��������� ������
#pragma endregion
    MPI_Barrier(MPI_COMM_WORLD);
    elapsed_time = -MPI_Wtime();

#pragma region EN
    // master process
#pragma endregion
#pragma region RU
    //������-�������
#pragma endregion
#pragma region UA
    //������� - ������
#pragma endregion
    if (pid == 0) {
        int index, i;
        elements_per_process = SIZE_ARRAY / size_p;

#pragma region EN
        // check if more than 1 processes are run
#pragma endregion
#pragma region RU
        // ���������, �������� �� ����� 1 ��������
#pragma endregion
#pragma region UA
        // ���������, �� �������� ����� 1 �������
#pragma endregion
        if (size_p > 1) {
#pragma region EN
    // distributes the portion of array
    // to child processes to calculate
    // their partial sums
#pragma endregion
#pragma region RU
    // ������������ ����� �������
    // �������� ��������� ��� ����������
    // ��������� ����
#pragma endregion
#pragma region UA
    // ��������� ������� ������
    // �������� �������� ��� ����������
    // ��������� ���
#pragma endregion
            double sum = 0;
            for (i = 1; i < size_p - 1; i++) {
                index = i * elements_per_process;

                MPI_Send(&elements_per_process,
                    1, MPI_DOUBLE, i, 0,
                    MPI_COMM_WORLD);

                MPI_Send(&array[index],
                    elements_per_process,
                    MPI_DOUBLE, i, 0,
                    MPI_COMM_WORLD);
            }

#pragma region EN
    // last process adds remaining elements
#pragma endregion
#pragma region RU
    // ��������� ������� ��������� ���������� ��������
#pragma endregion
#pragma region UA
    // �������� ������ ���� ���� ��������
#pragma endregion
            index = i * elements_per_process;
            int elements_left = SIZE_ARRAY - index;

            MPI_Send(&elements_left,
                1, MPI_DOUBLE,
                i, 0,
                MPI_COMM_WORLD);
            MPI_Send(&array[index],
                elements_left,
                MPI_DOUBLE, i, 0,
                MPI_COMM_WORLD);
        }

#pragma region EN
    // master process add its own sub array
#pragma endregion
#pragma region RU
    // ������� ������� ��������� ����������� ���������
#pragma endregion
#pragma region UA
    // �������� ������ ���� ������� �������
#pragma endregion
        double sum = 0;
        for (i = 0; i < elements_per_process; i++)
        {
            sum += array[i];
            //float timevalue = array[i] * x;
            //sum += factorial(array[i] - 1) / cos(timevalue) + log(x);

        }
#pragma region EN
    // collects partial sums from other processes
#pragma endregion
#pragma region RU
    // �������� ��������� ����� �� ������ ���������
#pragma endregion
#pragma region UA
    // ����� ������� ���� � ����� �������
#pragma endregion
        double tmp;
        for (i = 1; i < size_p; i++) {
            MPI_Recv(&tmp, 1, MPI_DOUBLE,
                MPI_ANY_SOURCE, 0,
                MPI_COMM_WORLD,
                &status);
            
            sum += tmp;
            //float timevalue = tmp * x;
            //sum += factorial(tmp - 1) / cos(timevalue) + log(x);


        }
#pragma region EN
    // stop the timer
#pragma endregion
#pragma region RU
    // ������������� ������
#pragma endregion
#pragma region UA
    // �������� ������
#pragma endregion
        elapsed_time += MPI_Wtime();

#pragma region EN
    // prints the final sum of array
#pragma endregion
#pragma region RU
    // ������� ������������� ����� �������
#pragma endregion
#pragma region UA
    // �������� ��������� ���� ������
#pragma endregion
        printf("Sum of array is : %f\n", sum);
        printf("\nTime: %f", elapsed_time);
    }
#pragma region EN
    // slave processes
#pragma endregion
#pragma region RU
    // ����������� ��������
#pragma endregion
#pragma region UA
    // ������������� �������
#pragma endregion
    
    else {
        MPI_Recv(&n_elements_recieved,
            1, MPI_DOUBLE, 0, 0,
            MPI_COMM_WORLD,
            &status);

#pragma region EN
    // stores the received array segment
    // in local array a2
#pragma endregion
#pragma region RU
    // ��������� ���������� ������� �������
    // � ��������� ������� a2
#pragma endregion
#pragma region UA
    // ������ ��������� ������� ������
    // � ���������� ����� a2
#pragma endregion
        MPI_Recv(&array_2, n_elements_recieved,
            MPI_DOUBLE, 0, 0,
            MPI_COMM_WORLD,
            &status);

#pragma region EN
    // calculates its partial sum
#pragma endregion
#pragma region RU
    // ��������� ��� ��������� �����
#pragma endregion
#pragma region UA
    // �������� ���� �������� ����
#pragma endregion
        double partial_sum = 0;
        for (int i = 0; i < n_elements_recieved; i++)
        {
            //partial_sum += array_2[i];

            float timevalue = array_2[i] * x;
            partial_sum += factorial(array_2[i] - 1) / cos(timevalue) + log(x);
        }
           
#pragma region EN
    // sends the partial sum to the root process
#pragma endregion
#pragma region RU
    // ���������� ��������� ����� ��������� ��������
#pragma endregion
#pragma region UA
    // ������ �������� ���� ���������� �������
#pragma endregion
        
        MPI_Send(&partial_sum, 1, MPI_DOUBLE,
            0, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}